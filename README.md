# go-anidex

Basic client for AniDex's upload API written in Golang

Quick and dirty, but it works

I'll come back and add some comments... eventually

```go
package main

import (
    anidex "gitlab.com/subsnotdubs/go-anidex"
)

func main() {
    c := anidex.NewClient("42069-y0uRK3YG0eShEr3")
    c.Upload("Uwuntu 20.04 LTS.iso.torrent", anidex.Other, anidex.English, &anidex.Options{
        Private: true,
    })
}
```
