package anidex

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type Client struct {
	apiKey string
}

type Category byte

const (
	AnimeSubbed      Category = 1
	AnimeRaw         Category = 2
	AnimeDubbed      Category = 3
	LiveActionSubbed Category = 4
	LiveActionRaw    Category = 5
	LightNovels      Category = 6
	MangaTranslated  Category = 7
	MangaRaw         Category = 8
	MusicLossy       Category = 9
	MusicLossless    Category = 10
	MusicVideo       Category = 11
	Games            Category = 12
	Applications     Category = 13
	Pictures         Category = 14
	AdultVideo       Category = 15
	Other            Category = 16
)

type Language byte

const (
	English  Language = 1
	Japanese Language = 2
	Chinese  Language = 21
	Korean   Language = 28
	Spanish  Language = 15
)

type Options struct {
	TorrentName string

	// Description of the torrent
	//
	// Supports a phpBB-like syntax:
	// 	[h1]Header 1[/h1]
	// 	...
	// 	[h5]Header 5[/h5]
	//
	// 	[b]Bold[/b]
	// 	[u]Underline[/u]
	// 	[i]Italic[/i]
	Description                      string
	GroupID                          uint
	Batch, Hentai, Reencode, Private bool
}

func NewClient(apiKey string) *Client {
	c := &Client{
		apiKey: apiKey,
	}
	return c
}

func (c *Client) Upload(filename string, cat Category, lang Language, opt *Options) (string, error) {
	buf := &bytes.Buffer{}

	w := multipart.NewWriter(buf)

	w.WriteField("api_key", c.apiKey)
	if opt.Description != "" {
		w.WriteField("description", opt.Description)
	}
	w.WriteField("subcat_id", strconv.Itoa(int(cat)))

	if opt.Batch {
		w.WriteField("batch", "1")
	}
	if opt.Hentai {
		w.WriteField("hentai", "1")
	}
	if opt.Reencode {
		w.WriteField("reencode", "1")
	}
	if opt.Private {
		w.WriteField("private", "1")
	}

	w.WriteField("group_id", strconv.Itoa(int(opt.GroupID)))
	w.WriteField("lang_id", strconv.Itoa(int(lang)))

	fw, err := w.CreateFormFile("file", filename)
	if err != nil {
		return "", err
	}

	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		return "", err
	}

	io.Copy(fw, f)

	if err := w.Close(); err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", "https://anidex.info/api/", buf)
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	return strings.TrimPrefix(string(body), "https://anidex.info/torrent/"), nil
}

